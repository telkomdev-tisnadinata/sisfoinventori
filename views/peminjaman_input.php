<div class="row">
          <div class="col-lg-12">
            <h1>Input Peminjaman <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Input Peminjaman</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          include "models/m_barang.php";
          include "models/m_peminjaman.php";
          $pjm = new Peminjaman($connection);
          $brg = new Barang($connection);
        
          if (isset($_POST['tambah'])) {
            $tambah = $pjm->tambah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Ditambahkan.';
            if (!$tambah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Menambahkan Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label for="barang">Pilih Barang</label>
                <select class="form-control" id="barang" name="barang">
                  <?php
                    $no = 1;
                    $tampil = $brg->tampil_filter('status_ketersediaan', 1);
                    if (!$tampil) {
                    ?>
                      <option>Tidak Dapat Mengambil Data Barang</option>
                    <?php
                    } else {
                      while($data = $tampil->fetch_assoc()){
                  ?>
                        <option value="<?php echo $data['kd_barang'].':'.$data['nama_barang'].':'.$data['jenis_barang']; ?>">
                          <?php echo $data['jenis_barang'].' - '.$data['nama_barang'].' ('.$data['kd_barang'].')'; ?>
                        </option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="tgl_pinjam">Tanggal Peminjaman</label>
                <input type="date" name="tgl_pinjam" class="form-control" id="tgl_pinjam" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="nip">NIP Peminjam</label>
                <input type="number" name="nip" class="form-control" id="nip" placeholder="NIP Peminjam "required>
              </div>
              <div class="form-group">
                <label class="control-label" for="nama_peminjam">Nama Peminjam</label>
                <input type="text" name="nama_peminjam" class="form-control" id="nama_peminjam" placeholder="Nama Peminjam" required>
              </div>
              <div class="form-group">
                <label>Divisi Kerja</label>
                <select class="form-control" name="divisi_kerja">
                  <option value="Bagian Teknik">Bagian Teknik</option>
                  <option value="Bagian Program">Bagian Program</option>
                  <option value="Bagian Berita">Bagian Berita</option>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="tujuan_peminjaman">Tujuan Peminjaman</label>
                <textarea class="form-control" rows="5" id="tujuan_peminjaman" name="tujuan_peminjaman" placeholder="Tujuan Peminjaman" Required></textarea>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <input type="submit" class="btn btn-success" name="tambah" value="TAMBAH">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
