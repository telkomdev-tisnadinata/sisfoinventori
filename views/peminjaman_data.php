<?php
include "models/m_peminjaman.php";
$pjm = new Peminjaman($connection);
?>
<div class="row">
          <div class="col-lg-12">
            <h1>Lihat Peminjaman <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Lihat Peminjaman</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Jenis Barang</th>
                            <th>Tanggal Pinjam</th>
                            <th>Nama Peminjam</th>
                            <th>NIP</th>
                            <th>Tujuan Peminjaman</th>
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $pjm->tampil();
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                        ?>
                            <form action="" method="post">
                                <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                                <tr>
                                    <td align="center"><?php echo $no++ ?></td>
                                    <td><?php echo $data->kd_barang; ?></td>
                                    <td><?php echo $data->nama_barang; ?></td>
                                    <td><?php echo $data->jenis_barang; ?></td>
                                    <td><?php echo $data->tgl_pinjam; ?></td>
                                    <td><?php echo $data->nama_peminjam.'<br>(Divisi '.$data->divisi_kerja.')'; ?></td>
                                    <td><?php echo $data->nip; ?></td>
                                    <td><?php echo $data->tujuan_peminjaman; ?></td>
                                </tr>
                            </form>
                        <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>