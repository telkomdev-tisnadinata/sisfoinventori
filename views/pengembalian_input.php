<div class="row">
          <div class="col-lg-12">
            <h1>Input Pengembalian <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Input Pengembalian</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          include "models/m_barang.php";
          include "models/m_peminjaman.php";
          include "models/m_pengembalian.php";
          $pbl = new Pengembalian($connection);
          $pjm = new Peminjaman($connection);
          $brg = new Barang($connection);
        
          if (isset($_POST['tambah'])) {
            $tambah = $pbl->tambah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Ditambahkan.';
            if (!$tambah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Menambahkan Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label for="barang">Pilih Barang Yang Dipinjam</label>
                <select class="form-control" id="barang" name="barang">
                  <?php
                    $no = 1;
                    $tampil = $brg->tampil_filter('status_ketersediaan', 0);
                    if (!$tampil) {
                    ?>
                      <option>Tidak Dapat Mengambil Data Barang</option>
                    <?php
                    } else {
                      while($data = $tampil->fetch_assoc()){
                        $peminjam = $pjm->tampil_filter('kd_barang', $data['kd_barang']);
                        $peminjam = $peminjam->fetch_object();
                  ?>
                        <option value="<?php echo $peminjam->id.':'.$peminjam->kd_barang.':'.$peminjam->nip; ?>">
                          <?php echo $data['jenis_barang'].' - '.$data['nama_barang'].' ('.$data['kd_barang'].')'.' - TANGGAL PINJAM : '.$peminjam->tgl_pinjam; ?>
                        </option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="tgl_pinjam">Tanggal Pengembalian</label>
                <input type="date" name="tgl_pengembalian" class="form-control" id="tgl_pengembalian" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="nip">NIP Peminjam</label>
                <input type="number" name="nip" class="form-control" id="nip" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="nama_peminjam">Status Pengembalian</label>
                <textarea class="form-control" rows="5" id="status_pengembalian" name="status_pengembalian"></textarea>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <input type="submit" class="btn btn-success" name="tambah" value="TAMBAH">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
