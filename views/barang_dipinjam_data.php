<?php
if (isset($_POST['ubah'])) {
    header("Location: ./?page=barang_edit&kd_barang=".$_POST['kd_barang']);
    die();
}

include "models/m_barang.php";
include "models/m_peminjaman.php";

$brg = new Barang($connection);
$pjm = new Peminjaman($connection);
?>
<div class="row">
          <div class="col-lg-12">
            <h1>Lihat Barang Dipinjam<small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Lihat Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Jenis Barang</th>
                            <th>NIP Peminjam</th>
                            <th>Nama Peminjam</th>
                            <th>Tanggal Dipinjam</th>
                            <th>Tujuan Dipinjam</th>
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $brg->tampil_filter('status_ketersediaan', 0);
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $peminjaman = $pjm->tampil_filter('kd_barang', $data->kd_barang);
                                $peminjaman = $peminjaman->fetch_object();
                        ?>
                            <form action="" method="post">
                                <input type="hidden" name="kd_barang" value="<?php echo $data->kd_barang; ?>"/>
                                <tr>
                                    <td align="center"><?php echo $no++ ?></td>
                                    <td><?php echo $data->kd_barang; ?></td>
                                    <td><?php echo $data->nama_barang; ?></td>
                                    <td><?php echo $data->jenis_barang; ?></td>
                                    <td><?php echo $peminjaman->nip; ?></td>
                                    <td><?php echo $peminjaman->nama_peminjam.'<br>(Divisi '.$peminjaman->divisi_kerja.')'; ?></td>
                                    <td><?php echo $peminjaman->tgl_pinjam; ?></td>
                                    <td><?php echo $peminjaman->tujuan_peminjaman; ?></td>
                                </tr>
                            </form>
                        <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>