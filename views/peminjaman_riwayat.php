<?php
include "../models/m_peminjaman.php";
include "../models/m_pengembalian.php";
require_once('../config/koneksi.php');
require_once('../models/database.php');

$connection = new Database($host, $user, $pass, $database);
$pjm = new Peminjaman($connection);
$pbl = new Pengembalian($connection);
?>
        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Data Barang</th>
                            <th>Jenis Barang</th>
                            <th>Data Peminjam</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tujuan Peminjaman</th>
                            <th>Tanggal Pengembalian</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $pjm->tampil_filter('kd_barang', $_GET['kd_barang']);
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $pengembalian = $pbl->tampil_filter('id_peminjaman', $data->id);
                                if (($pengembalian) && $pengembalian->num_rows > 0) {
                                    $pengembalian = $pengembalian->fetch_object();
                                    $tanggal_pengembalian = $pengembalian->tanggal_pengembalian;
                                    $status_pengembalian = 'SUDAH DIKEMBALIKAN';
                                    $keterangan = $pengembalian->status_pengembalian;
                                } else {
                                    $tanggal_pengembalian = '-';
                                    $status_pengembalian = 'BELUM DIKEMBALIKAN';
                                    $keterangan = '-';
                                }
                        ?>
                            <tr>
                                <td align="center"><?php echo $no++ ?></td>
                                <td><?php echo $data->nama_barang.' ('.$data->kd_barang.')'; ?></td>
                                <td><?php echo $data->jenis_barang; ?></td>
                                <td><?php echo $data->nama_peminjam.' ('.$data->nip.')'; ?></td>
                                <td><?php echo $data->tgl_pinjam; ?></td>
                                <td><?php echo $data->tujuan_peminjaman; ?></td>
                                <td><?php echo $tanggal_pengembalian; ?></td>
                                <td><?php echo $status_pengembalian; ?></td>
                                <td><?php echo $keterangan; ?></td>
                            </tr>
                        <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>