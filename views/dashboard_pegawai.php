<?php
include "models/m_barang.php";

$brg = new Barang($connection);
?>
<div class="row">
          <div class="col-lg-12">
            <h1>Pengelolaan Inventori <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Dashboard</a></li>
              <li class="active"><i class="icon-file-alt"></i> </li>
            </ol>
          </div>
        </div><!-- /.row -->
       
          <!-- Barang yang sedang dipinjam -->
          <div class="col-lg-3">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-check fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">3</p>
                    <p class="announcement-text">Barang Dipinjam</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Lihat Barang
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

        <div class="">
            <div class="col-lg-12">

            <!-- <?php
              $data = $brg->jumlah_barang();
              $data = $data->fetch_object();
              echo $data->jumlah;
            ?>  -->
            </div>
        </div>