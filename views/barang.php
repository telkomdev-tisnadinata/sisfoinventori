<div class="row">
          <div class="col-lg-12">
            <h1>Input Barang <small>Pegawai</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Input Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="form-group">
                  <label class="control-label" for="nama_barang">Nama Barang</label>
                  <input type="text" name="nama_barang" class="form-control" id="nama_barang" required>
                </div>
                <div class="form-group">
                  <label class="control-label" for="tgl_pinjam">Tanggal Pinjam</label>
                  <input type="date" name="tgl_pinjam" class="form-control" id="tgl_pinjam" required>
                </div>
                <div class="form-group">
                  <label class="control-label" for="nama_peminjam">Nama Peminjam</label>
                  <input type="text" name="nama_peminjam" class="form-control" id="nama_peminjam" required>
                </div>
                <div class="form-group">
                  <label class="control-label" for="nip">NIP</label>
                  <input type="number" name="nip" class="form-control" id="nip" required>
                </div>
                <div class="form-group">
                  <label class="control-label" for="tujuan_peminjaman">Tujuan Peminjaman</label>
                  <input type="text" name="tujuan_peminjaman" class="form-control" id="tujuan_peminjaman" required>
                </div>
                </div>
            <!-- Button simpan -->
            <div id="simpan" class="modal-footer">
              <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
