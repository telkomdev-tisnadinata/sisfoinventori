<div class="row">
          <div class="col-lg-12">
            <h1>Input Barang <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Edit Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          if (!isset($_GET['kd_barang'])) {
              header("Location: ./?page=barang_data");
              die();
          }
          include "models/m_barang.php";
          $brg = new Barang($connection);
        
          if (isset($_POST['ubah'])) {
            $ubah = $brg->ubah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Diubah.';
            if (!$ubah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Diubah Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
          $tampil = $brg->tampil($_GET['kd_barang']);
          if ($tampil) {
            $data = $tampil->fetch_assoc();
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label" for="kd_barang">Kode Barang</label>
                <input type="text" name="kd_barang" class="form-control" id="kd_barang" value="<?php echo $data['kd_barang'];?>" readonly>
              </div>
              <div class="form-group">
                <label class="control-label" for="nama_barang">Nama Barang</label>
                <input type="text" name="nama_barang" class="form-control" id="nama_barang" value="<?php echo $data['nama_barang'];?>" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="jenis_barang">Jenis Barang</label>
                <input type="text" name="jenis_barang" class="form-control" id="jenis_barang" value="<?php echo $data['jenis_barang'];?>" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="harga_barang">Harga Barang</label>
                <input type="number" name="harga_barang" class="form-control" id="harga_barang" value="<?php echo $data['harga_barang'];?>" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="spesifikasi_barang">Spesifikasi Barang</label>
                <textarea class="form-control" rows="5" id="spesifikasi_barang" name="spesifikasi_barang"><?php echo $data['spesifikasi_barang'];?></textarea>
              </div>
              <div class="form-group">
                <label class="control-label" for="tanggal_pembelian">Tanggal Pembelian</label>
                <input type="date" name="tanggal_pembelian" class="form-control" id="tanggal_pembelian" value="<?php echo $data['tanggal_pembelian'];?>" required>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <input type="submit" class="btn btn-success" name="ubah" value="UBAH">
            </div>
            </div>
        </form>

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>
