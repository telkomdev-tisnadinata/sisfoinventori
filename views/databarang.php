<?php
include "models/m_barang.php";

$brg = new Barang($connection);
?>
<div class="row">
          <div class="col-lg-12">
            <h1>Database Data Barang <small>Pegawai</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Data Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>Jenis Barang</th>
                            <th>Tanggal Pinjam</th>
                            <th>Nama Peminjam</th>
                            <th>NIP</th>
                            <th>Tujuan</th>
                            <!-- <th></th> -->                     
                        </tr>
                        <?php
                        $no = 1;
                        $tampil = $brg->tampil();
                        while($data = $tampil->fetch_object()){
                            ?>
                                            
                        <tr>
                            <td align="center"><?php echo $no++ ?></td>
                            <td><?php echo $data->nama_barang; ?></td>
                            <td><?php echo $data->jenis_barang; ?></td>
                            <td><?php echo $data->tgl_pinjam; ?></td>
                            <td><?php echo $data->nama_peminjam; ?></td>
                            <td><?php echo $data->nip; ?></td>
                            <td><?php echo $data->tujuan_peminjaman; ?></td>
                        </tr>
                        <?php
                        }?>
                    </table>
                </div>
            </div>
        </div>