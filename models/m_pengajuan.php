<?php
class Pengajuan {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }

    public function tampil($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tb_pengajuan p, tb_databarang d WHERE p.kd_barang = d.kd_barang";
        if($id != null){
            $sql .= " AND id = $id";
        }
        $sql .= " ORDER BY createdAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tb_pengajuan";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY createdAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tambah($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO tb_pengajuan(nama_pengaju,nip,divisi_kerja,kd_barang,keterangan)";
        $sql .= "VALUES('".$data['nama_pengaju']."',".$data['nip'].",'".$data['divisi_kerja']."','".$data['barang']."','".$data['keterangan']."')";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>