<?php
class Peminjaman {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }

    public function tampil($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tb_peminjaman";
        if($id != null){
            $sql .= " WHERE id = $id";
        }
        $sql .= " ORDER BY tgl_pinjam DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tb_peminjaman";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY tgl_pinjam DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tambah($data){
        $db = $this->mysqli->conn;
        $barang = explode(':',$data['barang']);
        $kd_barang = $barang[0];
        $nama_barang = $barang[1];
        $jenis_barang = $barang[2];
        $sql = "INSERT INTO tb_peminjaman(kd_barang,nama_barang,jenis_barang,tgl_pinjam,nama_peminjam,nip,tujuan_peminjaman,divisi_kerja)";
        $sql .= "VALUES('".$kd_barang."','".$nama_barang."','".$jenis_barang."','".$data['tgl_pinjam']."','".$data['nama_peminjam']."',".$data['nip'].",'".$data['tujuan_peminjaman']."','".$data['divisi_kerja']."')";
        $query = $db->query($sql) or ($db->error);
        if ($query) {
            $this->ubah_ketersediaan_barang($kd_barang, 0);
        }
        return $query;
    }
    public function ubah_ketersediaan_barang($kd_barang, $value){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tb_databarang SET status_ketersediaan = $value WHERE kd_barang = '$kd_barang'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>