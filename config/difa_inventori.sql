-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2019 at 05:05 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `difa_inventori`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_databarang`
--

CREATE TABLE `tb_databarang` (
  `kd_barang` varchar(10) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `jenis_barang` varchar(25) NOT NULL,
  `harga_barang` int(20) NOT NULL,
  `spesifikasi_barang` text NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `status_ketersediaan` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_databarang`
--

INSERT INTO `tb_databarang` (`kd_barang`, `nama_barang`, `jenis_barang`, `harga_barang`, `spesifikasi_barang`, `tanggal_pembelian`, `status_ketersediaan`) VALUES
('B001', 'SONY MARK A7S', 'CAMERA', 123123123, '-LENSA 12MM <br>\r\n-INFRARED ULTRA <br>\r\n-ZOOM 100x<br>', '2019-06-27', 0),
('B002', 'SONY MARK A7S', 'CAMERA', 123123123, '-LENSA 12MM <br>\r\n-INFRARED ULTRA <br>\r\n-ZOOM 100x<br>', '2019-06-27', 0),
('B003', 'SONY MARK A7S', 'CAMERA', 123123123, '-LENSA 12MM <br>\r\n-INFRARED ULTRA <br>\r\n-ZOOM 100x<br>', '2019-06-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_peminjaman`
--

CREATE TABLE `tb_peminjaman` (
  `id` int(11) NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `nama_barang` varchar(20) NOT NULL,
  `jenis_barang` varchar(20) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `nama_peminjam` varchar(50) NOT NULL,
  `nip` int(10) NOT NULL,
  `divisi_kerja` varchar(50) NOT NULL,
  `tujuan_peminjaman` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_peminjaman`
--

INSERT INTO `tb_peminjaman` (`id`, `kd_barang`, `nama_barang`, `jenis_barang`, `tgl_pinjam`, `nama_peminjam`, `nip`, `divisi_kerja`, `tujuan_peminjaman`) VALUES
(1, 'KMR01', 'Panasonic HC-MDH3', 'Kamera', '2019-06-01', 'Hendrik', 1301178490, '', '0'),
(2, 'KMR02', 'mic sony', 'mic', '2019-06-11', 'Abdul', 1301178492, '', 'Melakukan siaran ke papua selama 7 hari'),
(3, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(4, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(5, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(6, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(7, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(8, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(9, 'B002', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Adit', 1301178484, '', 'haha\r\nhehe'),
(10, 'B003', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Difa', 1301176969, '', 'haha\r\nhehe'),
(11, 'B003', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Difa', 1301176969, '', 'haha\r\nhehe'),
(12, 'B003', 'SONY MARK A7S', 'CAMERA', '2019-06-11', 'Difa', 1301176969, '', 'haha\r\nhehe'),
(13, 'B001', 'SONY MARK A7S', 'CAMERA', '0000-00-00', 'Fadli', 1301176969, '', 'Untuk vloging\r\nUntuk 3gp'),
(14, 'B001', 'SONY MARK A7S', 'CAMERA', '2019-07-01', 'Kodir', 131131131, '', 'Haha\r\nJawa\r\nTengah\r\nHaha\r\nJawa\r\nTengah\r\nHaha\r\nJawa'),
(15, 'B001', 'SONY MARK A7S', 'CAMERA', '2019-07-02', 'Jujur', 1982479827, '', 'aknfunskjnudgbdsjkcuhsd aj siuh jiasduajid ausdjia'),
(16, 'B001', 'SONY MARK A7S', 'CAMERA', '2019-07-06', 'Muhammad Aditya Tisnadina', 123, 'Bagian Program', 'huhuhu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengajuan`
--

CREATE TABLE `tb_pengajuan` (
  `id` int(11) NOT NULL,
  `nama_pengaju` varchar(100) NOT NULL,
  `nip` int(10) NOT NULL,
  `divisi_kerja` varchar(50) NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengajuan`
--

INSERT INTO `tb_pengajuan` (`id`, `nama_pengaju`, `nip`, `divisi_kerja`, `kd_barang`, `keterangan`, `createdAt`) VALUES
(3, 'dia', 123, 'Bagian Teknik', 'B001', 'cuma ngajuin aja iseng', '2019-07-06 14:06:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengembalian`
--

CREATE TABLE `tb_pengembalian` (
  `id` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `status_pengembalian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengembalian`
--

INSERT INTO `tb_pengembalian` (`id`, `id_peminjaman`, `tanggal_pengembalian`, `status_pengembalian`) VALUES
(3, 3, '2019-07-12', 'Baik dan Bagus');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `nip` varchar(10) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `nip_user` int(10) NOT NULL,
  `jenis_user` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_databarang`
--
ALTER TABLE `tb_databarang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indexes for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indexes for table `tb_pengajuan`
--
ALTER TABLE `tb_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pengembalian`
--
ALTER TABLE `tb_pengembalian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_pengajuan`
--
ALTER TABLE `tb_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pengembalian`
--
ALTER TABLE `tb_pengembalian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
